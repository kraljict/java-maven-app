#!/usr/bin/env groovy



pipeline {
    agent any
    tools {
        maven 'maven-3.8'
    }
    stages {
        stage("test") {
            steps {
                script {
                    echo "testing the application..."
                    sh "mvn test"
                }
            }
        }
        stage("increment version") {
            steps {
                script {
                    echo 'incrementing app version...'
                    sh "mvn build-helper:parse-version versions:set \
                        -DnewVersion=\\\${parsedVersion.majorVersion}.\\\${parsedVersion.minorVersion}.\\\${parsedVersion.nextIncrementalVersion}"
                    sh "mvn versions:commit"
                    def matcher = readFile('pom.xml') =~ '<version>(.+)</version>'
                    def version = matcher[0][1]
                    IMAGE_NAME = "$version-$BUILD_NUMBER"
                }
            }
        }
        stage("build jar") {
            steps {
                script {
                    echo "building the application..."
                    sh 'mvn clean package'
                }
            }
        }

        stage("build docker image") {
            steps {
                script {
                    echo "building the docker image..."
                    withCredentials([usernamePassword(credentialsId: 'nexus-credentials', passwordVariable: 'PASS',
                            usernameVariable: 'USER')]) {
                        sh "docker build -t 143.198.166.147:8083/${IMAGE_NAME} . "
                    }
                }
            }
        }

        stage("login to Nexus") {
            steps {
                script {
                    withCredentials([usernamePassword(credentialsId: 'nexus-credentials', passwordVariable: 'PASS',
                            usernameVariable: 'USER')]) {
                        sh "echo $PASS | docker login -u $USER --password-stdin 143.198.166.147:8083"
                    }
                }
            }
        }

        stage("deploy") {
            steps {
                script {
                    withCredentials([usernamePassword(credentialsId: 'nexus-credentials', passwordVariable: 'PASS',
                        usernameVariable: 'USER')]) {
                    sh "docker push 143.198.166.147:8083/${IMAGE_NAME}"
                    }
                }
            }
        }
        stage("commit version update") {
            steps {
                script {
                    withCredentials([usernamePassword(credentialsId: 'gitlab-credentials', passwordVariable: 'PASS',
                            usernameVariable: 'USER')]) {

                        sh "git remote set-url origin https://${USER}:${PASS}@gitlab.com/kraljict/java-maven-app.git"
                        sh 'git add .'
                        sh 'git commit -m "jenkins: version bump"'
                        sh 'git push origin HEAD:main'
                    }
                }
            }
        }
    }
}
